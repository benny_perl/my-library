import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import BookIcon from '@material-ui/icons/ImportContacts'
import CategoryIcon from '@material-ui/icons/Category'
import {Link } from "react-router-dom"

const useStyles = makeStyles(theme => ({
  root: {
    width: 350,
    height: 60,
    background: "#343a40",
    color: "white"
  },
  action: {
    color: "white"
  }
}))


export default function SimpleBottomNavigation() {
  const classes = useStyles()
  const [value, setValue] = React.useState(1)

  return (
    <BottomNavigation fixed="bottom"
      value={value}
      onChange={(_, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
    <BottomNavigationAction 
        label="Categories" 
        icon={<CategoryIcon />} 
        className={classes.action}
        component={Link}
        to="/categories" />
      <BottomNavigationAction 
        label="Books" 
        icon={<BookIcon />} 
        className={classes.action}  
        component={Link}
        to="/books" />
    </BottomNavigation>
  );
}
