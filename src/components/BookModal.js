import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import {connect} from 'react-redux'
import * as api from "../lib/api"
import {renderBookTable,bookSelectionChange} from '../lib/actions'


function MyVerticallyCenteredModal(props) {
    const {dispatch, ...myProps} = props
    const book = props.book[0]

    const categories = api.fetchCagoreis()

    let myNameRef = React.createRef()
    let myPriceRef = React.createRef()
    let myCategoryRef = React.createRef()
    let myAuthorRef = React.createRef()
 
    console.log("store: ",props.store)
    
    const addTheBook = () => {
        api.addBook({
            name: myNameRef.current.value,
            category:myCategoryRef.current.value,
            author:myAuthorRef.current.value,
            price:myPriceRef.current.value,
        })
        dispatch(renderBookTable(!props.render))
        props.onHide()
    }

    const editTheBook = () => {
        api.editBook(props.book,{
            name: myNameRef.current.value,
            category:myCategoryRef.current.value,
            author:myAuthorRef.current.value,
            price:myPriceRef.current.value,
        })
        dispatch(renderBookTable(!props.render))
        props.dispatch(bookSelectionChange([]))
        props.onHide()
    }
    
    const save = () =>{
        switch (props.action){
            case "add":
                myNameRef.current.value && myCategoryRef.current.value && myAuthorRef.current.value && myPriceRef.current.value ?
                addTheBook() :
                alert("fill ...")
                break
            case "edit":
                myNameRef.current.value && myCategoryRef.current.value && myAuthorRef.current.value && myPriceRef.current.value ?
                editTheBook():
                alert("fill ...")
                break
            default:
        }
    }


    return (
        <Modal
        {...myProps}
        size="lg"
        centered
        >
        <Modal.Header closeButton >
          <Modal.Title  style={{ flexGrow: "1", textAlign: "center" }}>
            {props.title}
          </Modal.Title>
        </Modal.Header>
            <Modal.Body>
                <div className="input-group form-row">
                    <div className="col-md-6">
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <div className="input-group-text">Name</div>
                            </div>
                            <input 
                                className="form-control"
                                type="text"
                                disabled= {props.action === "view"} 
                                defaultValue = 
                                    {(props.action === "view" || props.action === "edit") && book ? book.name : ""}
                                ref={myNameRef}
                                />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <div className="input-group-text">Category</div>
                            </div>
                            {props.action === "view" ?
                            <input 
                                className="form-control"
                                type="text"
                                disabled= {true} 
                                defaultValue = {book && book.category}
                                />:
                            <select className="custom-select" ref={myCategoryRef} >
                                <option value="0" hidden defaultValue >Choose...</option>
                                {categories && categories.map((category,index) => {
                                return (
                                    <option 
                                        selected={book && props.action==="edit" && category.name=== book.category } 
                                        key={index} 
                                        value={category.name}>{category.name}
                                    </option>)
                                }) }
                            </select>
                            }
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <div className="input-group-text">Author</div>
                            </div>
                            <input 
                                className="form-control"
                                type="text"
                                disabled= {props.action === "view"} 
                                defaultValue = 
                                    {(props.action === "view" || props.action === "edit") && book ? book.author : ""}
                                ref={myAuthorRef}
                                />
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <div className="input-group-text">price</div>
                            </div>
                            <input 
                                className="form-control"
                                type="text"
                                disabled= {props.action === "view"} 
                                defaultValue = 
                                    {(props.action === "view" || props.action === "edit") && book ? book.price :""}
                                ref={myPriceRef}
                            />
                        </div>
                    </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={save}>Save</Button>
                <Button onClick={props.onHide}>Cencel</Button>
            </Modal.Footer>
    </Modal>
    );
  }
  const mapStateToProps = state => ({
    book: state.selectedBook,
    render: state.bookTableRender,
    store: state
  })
  
  export default connect(mapStateToProps)(MyVerticallyCenteredModal)