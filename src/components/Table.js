import React from 'react'
import MaterialTable from 'material-table'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import SearchIcon from '@material-ui/icons/Search'
import clearIcon from '@material-ui/icons/Clear'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import LastPageIcon from '@material-ui/icons/LastPage'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import {connect} from 'react-redux'
import {bookSelectionChange, categorySelectionChange, Views} from '../lib/actions'


function MaterialTableDemo(props) {
  return (
    <MaterialTable
      title= {props.title}
      columns={props.columns}
      data={props.data} 
      icons = {{
        SortArrow: ArrowUpwardIcon,
        FirstPage: FirstPageIcon,
        LastPage: LastPageIcon,
        NextPage: ChevronRightIcon,
        PreviousPage: ChevronLeftIcon,
        ResetSearch: clearIcon,
        Search: SearchIcon,
        DetailPanel: ChevronRightIcon 
        }}
        options={props.options}
        onSelectionChange={props.currentView === Views.BookView ? 
        rows => props.dispatch(bookSelectionChange(rows)):
        rows => props.dispatch(categorySelectionChange(rows))}
      />
  )
}

const mapStateToProps = state => ({
  currentView: state.currentView
})

export default connect(mapStateToProps)(MaterialTableDemo)