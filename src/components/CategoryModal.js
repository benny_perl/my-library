import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import {connect} from 'react-redux'
import * as api from "../lib/api"
import {renderCategoryTable,categorySelectionChange} from '../lib/actions'


function MyVerticallyCenteredModal(props) {
    const {dispatch, ...myprops} = props
    console.log("store: ",props.store)
    let myNameRef = React.createRef()

    const addTheCategory = () => {
      api.addCategory({
          name: myNameRef.current.value,
      })
      dispatch(renderCategoryTable(!props.render))
      props.onHide()
  }

  const editTheCategory = () => {
      api.editCategory(props.book,{
          name: myNameRef.current.value,
      })
      dispatch(renderCategoryTable(!props.render))
      props.dispatch(categorySelectionChange([]))
      props.onHide()
  }

  const save = () =>{
    switch (props.action){
        case "add":
            myNameRef.current.value ?
            addTheCategory() :
            alert("fill ...")
            break
        case "edit":
            myNameRef.current.value ?
            editTheCategory():
            alert("fill ...")
            break
        default:
    }
  }
    return (
        <Modal
        {...myprops}
        size="lg"
        centered
        >
        <Modal.Header closeButton >
          <Modal.Title  style={{ flexGrow: "1", textAlign: "center" }}>
            {props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="col-md-6">
                <div className="input-group mb-2">
                    <div className="input-group-prepend">
                        <div className="input-group-text">Name</div>
                    </div>
                    <input 
                        className="form-control"
                        type="text"
                        disabled= {props.action === "view"} 
                        defaultValue = 
                            {(props.action === "view" || props.action === "edit") && props.category[0] ? 
                            props.category[0].name : ""}
                        ref={myNameRef}
                    />
                </div>
            </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={save}>Save</Button>
          <Button onClick={props.onHide}>Cencel</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  const mapStateToProps = state => ({
    category: state.selectedCategory,
    render: state.categoryTableRender,
    store: state
  })
  
  export default connect(mapStateToProps)(MyVerticallyCenteredModal)