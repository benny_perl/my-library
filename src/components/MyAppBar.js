import React from 'react'
import {  makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import EditIcon from '@material-ui/icons/Edit'
import InfoIcon from '@material-ui/icons/Info'
import BookModal from './BookModal'
import CategoryModal from './CategoryModal'
import {connect} from 'react-redux'
import {Views, renderBookTable, renderCategoryTable, bookSelectionChange, categorySelectionChange} 
  from '../lib/actions'
import * as api from "../lib/api"

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
    },
  },
}))

function PrimarySearchAppBar(props) {
  const classes = useStyles()
  const [modalShow, setModalShow] = React.useState(false)
  const [action, setAction] = React.useState("")
  const [title, setTitle] = React.useState("")

  const removeBook = () => {
    props.dispatch(renderBookTable(!props.bookRender)) 
    api.removeBook(props.selectedBook)
    props.dispatch(bookSelectionChange([]))
  }

  const removeCategory = () => {
    props.dispatch(renderCategoryTable(!props.categoryRender)) 
    api.removeCategory(props.selectedCategory)
    props.dispatch(categorySelectionChange([]))
  }

  return (
    <div className={classes.grow}>
      <AppBar className="bg-dark" position="static" >
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            My Library 
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton 
              color="inherit" 
              onClick={() => {
                setAction("view")
                setTitle(props.currentView === Views.BookView ? "Book info" : "Category info")
                setModalShow(true)}}>
              <InfoIcon />
            </IconButton>
            <IconButton 
              color="inherit"
              onClick={() => {
                setAction("edit")
                setTitle(props.currentView === Views.BookView ?"edit Book info": "edit category info")
                setModalShow(true)}}>
              <EditIcon />
            </IconButton>
            <IconButton color="inherit"
                onClick={() => {
                  props.currentView === Views.BookView ?
                  removeBook() :
                  removeCategory()
                }}
            >
              <RemoveCircleOutlineIcon />
            </IconButton>
            <IconButton color="inherit"
                onClick={() => {
                  setAction("add")
                  setTitle(props.currentView === Views.BookView ? "Adding a new book": "adding new category")
                  setModalShow(true)}}>
              <AddCircleOutlineIcon />
            </IconButton>
            { props.currentView === Views.BookView ?
            <BookModal 
              show= {modalShow} 
              title={title} 
              action={action} 
              onHide={() => setModalShow(false)}
            /> :
            <CategoryModal 
              show= {modalShow} 
              title={title} 
              action={action} 
              onHide={() => setModalShow(false)}
            />}
          </div>
        </Toolbar>
      </AppBar>
    </div>
  )
}
const mapStateToProps = state => ({
  currentView: state.currentView,
  selectedBook: state.selectedBook,
  selectedCategory: state.selectedCategory,
  bookRender: state.bookTableRender,
  categoryRender: state.categoryTableRender,
})

export default connect(mapStateToProps)(PrimarySearchAppBar)