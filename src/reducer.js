import {CATEGORY_SELECTION_CHANGE,BOOK_SELECTION_CHANGE, SET_CURRENT_VIEW,
        RENDER_BOOK_TABLE,RENDER_CATEGORY_TABLE} 
    from './lib/actions'

export default function reducer(state =[], {type, payload}) {
     switch (type) {
        case BOOK_SELECTION_CHANGE:
            return {...state, ...payload}
        case CATEGORY_SELECTION_CHANGE:
            return {...state, ...payload}
        case SET_CURRENT_VIEW:
            return {...state, ...payload}
        case RENDER_BOOK_TABLE:
            return {...state,...payload}
        case RENDER_CATEGORY_TABLE:
            return {...state,...payload}
        default:
            return state
    }
}