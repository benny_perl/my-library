import React,{useEffect} from 'react'
import './App.css'
import MyBottomNavigation from "./components/MyBottomNavigation"
import MyAppBar from "./components/MyAppBar"
import BooksView from "./views/BooksView"
import CategoriesView from "./views/CategoriesView"
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom"
import {bookList, categoryList} from "./lib/data"

function App() {

  useEffect(() => {
    localStorage.setItem('categoryList', JSON.stringify(categoryList));
    localStorage.setItem('bookList', JSON.stringify(bookList));
  });


  return (
    <div className="App">
      <div className="container p-0 d-flex flex-column flex-grow-1 align-items-center bg-secondary">
        <div className="w-100">
          <MyAppBar/>
        </div> 
        <Router>
          <div className="flex-grow-1  d-inline-flex flex-column justify-content-center" style={{minWidth:"57.5%"}}>
            <Route path="/categories" component={CategoriesView} />
            <Route path="/books"  component={BooksView} />
            <Redirect exact from="/" to="/books" />
          </div> 
          <div >
            <MyBottomNavigation/>
          </div>
        </Router> 
      </div>
    </div>
  )
}

export default App
