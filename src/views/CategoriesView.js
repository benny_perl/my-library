import React from 'react'
import Table from "../components/Table"
import {setCurrentView, Views} from '../lib/actions'
import {connect} from 'react-redux'
import * as api from "../lib/api"

function CategoriesView(props) {
    props.dispatch(setCurrentView(Views.CategoryView))


    return (
        <div style={{width:"100%"}}>   
            <Table title="All categories"
                columns = {[
                    { title: 'Name', field: 'name' },
                ]}
                data= {api.fetchCagoreis()}
                options={{grouping: false, selection: true  }}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    render: state.categoryTableRender
})

export default connect(mapStateToProps)(CategoriesView)
