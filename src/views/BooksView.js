import React from 'react'
import Table from "../components/Table"
import {setCurrentView, Views} from '../lib/actions'
import {connect} from 'react-redux'
import * as api from "../lib/api"

function BooksView(props) {
    props.dispatch(setCurrentView(Views.BookView))


    return (
        <div style={{width:"100%"}}>   
            <Table title="All books"
                columns = {[
                    { title: 'Name', field: 'name' },
                    { title: 'Category', field: 'category' },
                    { title: 'Author', field: 'author' },
                    { title: 'Price', field: 'price', type: 'numeric'},
                ]}
                data= {api.fetchBooks()}
                options={{grouping: true, selection: true }}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    render: state.bookTableRender
})

export default connect(mapStateToProps)(BooksView)

