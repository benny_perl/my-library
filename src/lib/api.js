export function fetchBooks() {
  return JSON.parse(localStorage.getItem('bookList'))
}

export function fetchCagoreis() {
  return JSON.parse(localStorage.getItem('categoryList'))
}

export function addBook(newBook) {
  const bookList = JSON.parse(localStorage.getItem('bookList'))
  bookList.push(newBook)
  localStorage.setItem('bookList', JSON.stringify(bookList))
}

export function removeBook(book) {
  const bookList = JSON.parse(localStorage.getItem('bookList'))
  const newBookList = bookList.filter( value => {
    return value.name !== book[0].name})
  localStorage.setItem('bookList', JSON.stringify(newBookList))
}

export function editBook(book, newBook) {
  removeBook(book)
  addBook(newBook)
}

export function addCategory(newCategory) {
  const categoryList = JSON.parse(localStorage.getItem('categoryList'))
  categoryList.push(newCategory)
  localStorage.setItem('categoryList', JSON.stringify(categoryList))
}

export function removeCategory(category) {
  const categoryList = JSON.parse(localStorage.getItem('categoryList'))
  const newCategoryList = categoryList.filter( value => {
    return value.name !== category[0].name})
  localStorage.setItem('categoryList', JSON.stringify(newCategoryList))
  }

export function editCategory(category, newCategory) {
  removeBook(category)
  addBook(newCategory)
}