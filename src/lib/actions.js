export const BOOK_SELECTION_CHANGE = "bookSelectionChange"
export const CATEGORY_SELECTION_CHANGE = "categorySelectionChange"
export const SET_CURRENT_VIEW = "setCurrentView"
export const RENDER_BOOK_TABLE = "renderBookTable"
export const RENDER_CATEGORY_TABLE = "rendercategoryTable"

export const Views = {
    BookView:"books",
    CategoryView:"categories"
}

export function renderBookTable(row){
    return {
        type:RENDER_BOOK_TABLE,
        payload: {
            bookTableRender:row
        }
    }
}

export function renderCategoryTable(row){
    return {
        type:RENDER_CATEGORY_TABLE,
        payload: {
            categoryTableRender:row
        }
    }
}

export function bookSelectionChange(row){
    return {
        type:BOOK_SELECTION_CHANGE,
        payload: {
            selectedBook:row
        }
    }
}

export function categorySelectionChange(row){
    return {
        type:CATEGORY_SELECTION_CHANGE,
        payload: {
            selectedCategory:row
        }
    }
}
export function setCurrentView(view){
    return {
        type:SET_CURRENT_VIEW,
        payload: {
            currentView:view
        }
    }
}