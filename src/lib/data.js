export const bookList =[
    {
        name: "Harry Potter and the Deathly Hallows",
        category: "Kids",
        price: "7.29",
        author: "J.K. Rowling"
    },
    {
        name: "Eat to Live",
        category: "Cooking",
        price: "4.49",
        author: "Joel Fuhrman"
    }
]

export const categoryList =[
    {
        name: "Kids"
    },
    {
        name: "Cooking"
    }
]