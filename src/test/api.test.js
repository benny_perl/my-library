import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as api from '../lib/api'

it('adding a new book', () => {
  const div = document.createElement('div')
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});